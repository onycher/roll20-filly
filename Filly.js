

var Filly;
(function (Filly) {
    "use strict";
    var metadata = {
        name: 'Filly',
        version: '0.1.0'
    };
    var attr_name_id = 'filly_id';
    var attr_name_status = 'filly_status'

    function getWhisperTarget(id) {
        let targets = [getObj('player', id)];
        if (targets[0]) {
            return '/w ' + targets[0].get('displayname').split(' ')[0] + ' ';
        }
        return '';
    }


    function get_mount(token) {
        if (token.get('represents') === '') {
            return
        }
        var mount_id = getAttrByName(token.get('represents'), attr_name_id)
        if (mount_id === undefined) {
            return
        }
        var mount = findObjs({ type: 'graphic', represents: mount_id, pageid: token.get('pageid') });
        if (mount.length !== 1) {
            return;
        }
        return mount[0];
    }

    function move_mount(obj, prev) {
        var mount = get_mount(obj)
        var status = getAttrByName(obj.get('represents'), attr_name_status)
        if (status === undefined) {
            return
        }
        if (status != 'mounted') {
            return
        }
        if (mount === undefined) {
            return
        }
        mount.set('top', obj.get('top'))
        mount.set('left', obj.get('left'))
        toFront(mount)
        toFront(obj)

    }

    function assign(token, mount_id) {
        var char_id = token.get('represents');
        var attr = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_id })
        if (attr.length === 0) {
            createObj('attribute', { characterid: char_id, name: attr_name_id, current: mount_id })
        }
        else {
            attr[0].set('current', mount_id)
        }
        attr = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_status })
        if (attr.length === 0) {
            createObj('attribute', { characterid: char_id, name: attr_name_status, current: 'dismounted' })
        }
        else {
            attr[0].set('current', 'dismounted')
        }
        return 'Mount succesfully assigned'
    }

    function unassign(token, mount_id) {
        var char_id = token.get('represents');
        var attr = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_id })
        if (attr.length !== 1) {
            return 'Too many attributes named ' + attr_name_id
        }
        else {
            attr[0].remove();
        }
        attr = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_status })
        if (attr.length !== 1) {
            return 'Too many attributes named ' + attr_name_status
        }
        else {
            attr[0].remove();
        }
        return 'Mount succesfully unassigned'
    }

    function mount(token) {
        var char_id = token.get('represents')
        var status = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_status })
        if (status.length !== 1) {
            return 'Too many attributes with name ' + attr_name_status
        }
        status[0].set('current', 'mounted')
        move_mount(token);
        return 'You are now mounted'
    }

    function dismount(token) {
        var char_id = token.get('represents')
        var status = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_status })
        if (status.length !== 1) {
            return 'Too many attributes with name ' + attr_name_status
        }
        status[0].set('current', 'dismounted')
        return ' You are now not mounted'
    }

    function summon(owner) {
        var char_id = owner.get('represents')
        var status = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_status })
        if (status.length !== 1) {
            return 'Too many attributes named '+attr_name_status
        }
        status[0].set('current', 'dismounted')
        var mount_id = getAttrByName(char_id, attr_name_id)
        if (mount_id === undefined) {
            return 'No attribute named attr_name_id'
        }
        var mount = getObj('character', mount_id)
        if (mount === undefined) {
            return 'Could not find character representing your mount'
        }
        var ret = mount.get('defaulttoken', function (token) {
            var tok = JSON.parse(token);
            tok.pageid = owner.get('pageid');
            var imageSource = tok.imgsrc;
            const parts = imageSource.match(/(.*\/images\/.*)(thumb|med|original|max)(.*)$/);
            if (parts) {
                imageSource = parts[1] + 'thumb' + parts[3];
            }
            tok.imgsrc = imageSource;
            tok.top = owner.get('top');
            tok.left = owner.get('left');
            return createObj('graphic', tok)

        })
        if (ret === undefined) {
            return 'imgsrc must be in your library'
        }
        return 'Mount summoned'
            

    }


    function dismiss(token) {
        var char_id = token.get('represents')
        var status = findObjs({ type: 'attribute', characterid: char_id, name: attr_name_status })
        if (status.length !== 1) {
            return 'Too many attributes named '+attr_name_status
        }
        status[0].set('current', 'dismissed')
        var mount = get_mount(token)
        if (mount === undefined) {
            return 'Could not find a mount';
        }
        mount.remove()
        return 'Mount dissmised'
    }

    function handleInput(msg_orig) {
        var msg = _.clone(msg_orig);
        if (msg.type !== 'api') {
            return;
        }
        if (msg.content.match(/^!Filly \w/)) {
            var result = undefined
            var args = msg.content.split(' ');
            if (args.length == 2) {
                if (msg.selected === undefined) {
                    sendChat('Filly', getWhisperTarget(msg.playerid)+'No character selected');
                    return;
                }
                if (msg.selected.length !== 1) {
                    sendChat('Filly', getWhisperTarget(msg.playerid)+'Only one character should be selected');
                    return;
                }
                var selected = msg.selected[0];
                var token = getObj(selected._type, selected._id);
                args = _.object(['prefix', 'cmd'], args)
                if (args.cmd === 'mount') {
                    result = mount(token);
                }
                else if (args.cmd === 'dismount') {
                    result = dismount(token)
                }
                else if (args.cmd === 'summon') {
                    try {
                        result = summon(token)
                    }
                    catch(err) {
                        result = err 
                    }
                }
                else if (args.cmd === 'dismiss') {
                    result = dismiss(token)
                }
            }
            else if (args.length == 4) {
                if (!playerIsGM(msg.playerid)) {
                    sendChat('Filly', getWhisperTarget(msg.playerid) + 'Only GM can use this script');
                    return    
                }
                args = _.object(['prefix', 'cmd', 'token_id', 'mount_id'], args)
                var token = getObj('graphic', args.token_id)
                if (args.cmd === 'assign') {
                    result = assign(token, args.mount_id)
                }
                else if (args.cmd === 'unassign') {
                    result = unassign(token, args.mount_id)
                }
            }
            else {
                sendChat(getWhisperTarget(msg.playerid)+'Wrong number of atributes');
                return
            }
            sendChat('Filly', getWhisperTarget(msg.playerid)+result)
        }
    }
    function registerEventHandlers() {
        on('chat:message', handleInput);
        on('change:graphic', move_mount);
    }
    function checkInstall() {
        log(metadata.name + ' ' + metadata.version + ' installed')
    }
    Filly.registerEventHandlers = registerEventHandlers;
    Filly.checkInstall = checkInstall;
})(Filly || (Filly = {}));

on('ready', function () {
    Filly.registerEventHandlers();
    Filly.checkInstall();
});